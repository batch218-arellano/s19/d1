// console.log("Hello World");

// What are conditional statements?
// Conditional statements allow us to control the flow of our program and it allow us to run statement/instruction based on the condition.

// if statement
// Executes a statement if a specified condition is true
/*
    Syntax:
    if(condition){
        code block / statements;
    }

*/

let numA = 0;

if(numA < 0){
    console.log("Hello");
}
// The codeblock inside the if statement will not work if it doesn't satisfy the condition

if(numA == 0){
    console.log("Hello");
}
// The result of the expression must result to "true", else it will not run the statement inside.
console.log(numA < 0); //false
console.log(numA == 0); //true

let city = "New York";

if(city == "New York"){
    console.log("Welcome to New York City");
}

// else if clause
/*
    - Executes a statement if previous condition/s are false and if the specified condition is true.
    - The "else if" clause is optional and can be added to capture additional conditions to change the flow of the program.
*/
// city =  "New York";
if(city === "New York"){
    console.log("Welcome to New York City!");
}
else if(city ==="Tokyo"){
    console.log("Welcome to Tokyo, Japan");
}

let numH = 0;

if(numH<0){ //returned false
    console.log("The number is negative");
}
else if(numH>0){ // returned false
    console.log("The number is positive");
}
else if(numH==0){
    console.log("The number is zero");
}


// Alternative code using else statement
if(numH<0){ //returned false
    console.log("The number is negative");
}
else if(numH>0){ // returned false
    console.log("The number is positive");
}
else{
    console.log("The number is zero");
}

// else statement
/*
    - Executes a statent if all other condition are false.
    - The 'else' statement is optional and can be added to capture any other result to change the flow of the program
*/

// city = New York;

if(city === "New York"){
    console.log("Welcome to New York City!");
}
else if(city ==="Tokyo"){
    console.log("Welcome to Tokyo, Japan");
}
else{
    console.log("City is not included in the list.");
}

/*
let username = "admin";
let password = "admin1234";

if(username == "admin"){
    console.log("Correct username");
}
if(password == "admin1234"){
    console.log("Correct password");
}
else{
    console.log("Wrong username or password");
}

if(username == "admin" && password == "admin1234"){
    console.log("Successful Login");
}
else{
    console.log("Wrong username or password");
}*/

let message = "No message";
    console.log(message);

    function determineTyphooneIntensity(windSpeed){

        if(windSpeed < 30){
            return "Not a typhoon yet."
        }
        else if(windSpeed <= 61){
            return "Tropical depression detected."
        }
        else if (windSpeed >= 62 && windSpeed <= 88){
            return "Tropical storm detected."
        }
        else if(windSpeed >= 89 && windSpeed <= 117){
            return "Severe Tropical storm deteceted";
        }
        else{
            return "Typhoon detected."
        }
    }

    message = determineTyphooneIntensity(110);
    console.log(message);

    //  console.warn() is a good way to print warnings in our console that could help us developers act on certain output within our code.
    if(message === "Severe Tropical storm deteceted"){
        console.warn(message);
    }

// [SECTION] Truthy and False
/*
    - In javascript a "truthy" value that is considered true when encountered in a boolean Context
    - False
        1. false
        2. 0
        3. ""
        4. null
        5. undefined // let number; 
        6. NaN (Not a Number)

*/
let isMarried = true;
// Truthy examples:
if(true){
    console.log("Truthy");
}
if(1){
    console.log("Truthy");
}
if([]){
    console.log("Truthy");
}

// Falsy examples
if(false){
    console.log("Falsy");
}
if(0){
    console.log("Falsy");
}
if(undefined){
    console.log("Falsy");
}
if(isMarried){
    console.log("Truthy");
}

// [SECTION] Conditional (Ternary) Operator
/*
    - The Conditional (Ternary) operator takes in three operands
*/

let ternaryResult = (1 < 18)? true : false;
console.log("Result of Ternary Operator: " + ternaryResult);

// Multiple statement execution using ternary operator

let name;

function isOfLegalAge(){
    name = "John";
    return "You are in legal age";
}

function underAge(){
    name = "Jane";
    return "You are under age limit";
}

// parseInt - converts the input received into a number datatype
    // if a input is not a number, it will return NaN (Not a Number)
let age = parseInt(prompt("What is your age"));
console.log(age);

let legalAge = (age>=18)? isOfLegalAge() : underAge();
console.log("Return of Ternary Operator in functions " + legalAge + ', ' + name);


// [SECTION] Switch Statements

let day = prompt ("What day of the week is it today").toLowerCase(); // "toLowerCase()" - to be not case sensitive 
console.log (day) ; //display input

switch(day){
 	case "monday": //case sensitive
  	console.log ("The color of the day is red."); //display output

  	break; //to put a 'pause /stop' once met.

  	case "tuesday":
  	console.log("The color of the day is orange.");

  	break;

  	case "wednesday":
  	console.log("The color of the day is yellow.");
  	break;	
  	case "thursday":
  	console.log("The color of the day is green.");
	break;
  	case "friday":
  	console.log("The color of the day is blue.");
  	break;
  	case "saturday":
  	console.log("The color of the day is indigo.");
  	break;
  	case "sunday":
  	console.log("The color of the day is violet.");

  	default: // - for other responses
	console.log("Please input a valid day");
	break;

}

/* let age = prompt ("age: ");
age = parseInt	(age);

switch(age){
	case 18:
   console.Log ("Age is valid");

	case 17:
	console.Log ("Age is valid");
	
	break;
	
	case 19:
	console.Log ("Age is valid");

	break;
	
	case 18:
	console.Log ("Give freebies");

}

*/

// [SECTION] Try-Catch-Finally Statement

let codeErrorMessage = "Code Error";
						//parameter
function showIntensityAlert(windSpeed){
	try{
		// codes /  code block to try
		alerat(determineTyphooneIntensity(windSpeed));
	}

	catch(error){
		// error.message is used to access the information relating to an error object
		console.warn(error.message);
	}
	finally{
		alert("Intensity updates will show new alert.");
	}
}

showIntensityAlert(56);

console.log("Sample Output after try-catch-finally statemnt block")



